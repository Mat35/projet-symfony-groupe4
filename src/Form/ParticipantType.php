<?php

namespace App\Form;

use App\Entity\Participant;
use App\Entity\Site;
use App\Repository\SiteRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ParticipantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'label'  => 'Pseudo',
                'required'  => true,
                'attr'  => [
                    'placeholder' => 'pseudo',
                ]

                ]
            )
            ->add('nom', TextType::class, [
                'label'  => 'Nom',
                'required'  => true,
                'attr'  => ['placeholder' => 'nom']
                ]
            )
            ->add('prenom', TextType::class, [
                'label'  => 'Prénom',
                'required'  => true,
                'attr'  => ['placeholder' => 'prénom']
                ]
            )
            ->add('telephone', TelType::class, [
                'label'  => 'Téléphone',
                'required'  => true,
                'attr'  => ['placeholder' => 'numéro de téléphone']
                ]
            )
            ->add('mail', EmailType::class, [
                'label'  => 'Email',
                'required'  => true,
                'attr'  => ['placeholder' => 'mail']
                ]
            )
            ->add('password', RepeatedType::class, [
                'type'              =>PasswordType::class,
                'invalid_message'   =>'La confirmation du mot de passe est fausse',
                'required'          =>true,
                'first_options'     =>[ 'label' =>'Mot de passe'],
                'second_options'     =>[ 'label' =>'Confirmation'],
                ])

             ->add('site', EntityType::class, array(
                'class' => Site::class,
                'choice_label' => 'nom',
             ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Participant::class,
        ]);
    }
}
