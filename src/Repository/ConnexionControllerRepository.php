<?php

namespace App\Repository;

use App\Entity\ConnexionController;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ConnexionController|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConnexionController|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConnexionController[]    findAll()
 * @method ConnexionController[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConnexionControllerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConnexionController::class);
    }

    // /**
    //  * @return ConnexionController[] Returns an array of ConnexionController objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ConnexionController
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
